Feature: api Connection

  Scenario: 200 Response from request
    When I request "/api/bikes"
    Then the response code is 200


  Scenario: Je peux recuperer un velo
    Given the "Accept" request header is "application/json"
    When I request "/api/bikes/1"
    Then the response code is 200
    Then the response body is:
      """
      {"id":1,"frontWheel":"\/api\/wheels\/1","backWheel":"\/api\/wheels\/1","frame":"\/api\/frames\/1","handlebar":"\/api\/handlebars\/1","name":"My Bike","total":437.45}
      """

  Scenario: Je peux filtrer les velos par le materiel du cadre
    Given the "Accept" request header is "application/json"
    When I request "/api/bikes?frame.material=alu"
    Then the response code is 200
    Then the response body is:
      """
      [{"id":1,"frontWheel":"\/api\/wheels\/1","backWheel":"\/api\/wheels\/1","frame":"\/api\/frames\/1","handlebar":"\/api\/handlebars\/1","name":"My Bike","total":437.45}]
      """

  Scenario: Je peux creer un velo
    Given the request body is:
    """
    {
      "frontWheel": "/api/wheels/1",
      "backWheel": "api/wheels/1",
      "frame": "/api/frames/2",
      "handlebar": "/api/handlebars/2",
      "name": "My Bike 2"
    }
    """
    And the "Content-Type" request header is "application/json"
    When I request "/api/bikes" using HTTP POST
    Then the response code is 201

