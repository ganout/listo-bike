# language: fr

  Fonctionnalité: création d'une facture
    Scénario: lorsque je demande une facture, elle s'édite
      Etant donné que je choisi une roue avant du velo en aluminium avec un prix de 13.99 €
      Et que je choisi une roue arrière du velo en aluminium avec un prix de 13.99 €
      Et que je choisi un cadre du velo en aluminium avec un prix de 54.99 €
      Et que je choisi un guidon du velo en aluminium avec un prix de 28.99 €
      Alors je reçois ma facture avec un montant total de 134.352 €

    Scénario: lorsque je demande une facture, elle s'édite
      Alors je reçois ma facture avec un montant total de 0 €