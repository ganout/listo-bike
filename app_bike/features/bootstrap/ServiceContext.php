<?php


use App\Entity\Bike;
use App\Service\BillService;
use Behat\Behat\Context\Context;
use Behat\Behat\Hook\Call\BeforeScenario;

class ServiceContext implements Context
{


    /**
     * @var BillService
     */
    private BillService $billService;
    private Bike $bike;
    private FeatureContext $featureContext;

    public function __construct(BillService $billService)
    {
        $this->billService = $billService;
        $this->bike = new Bike();
    }

    /**
     * @BeforeScenario
     */
    public function beforeScenario(\Behat\Behat\Hook\Scope\BeforeScenarioScope $beforeScenario)
    {
        $env = $beforeScenario->getEnvironment();
        $this->featureContext = $env->getContext(FeatureContext::class);
    }

    /**
     * @Then je reçois ma facture avec un montant total de :arg1 €
     */
    public function jeRecoisMaFactureAvecUnMontantTotalDeEur(string $arg1)
    {
        $bill = $this->billService->billGenerator($this->featureContext->getBike());
        if (strcmp($bill["total"], $arg1) != 0) {
            throw new Exception("le montant total de la facture ne correspond pas !!!");
        }
    }

}