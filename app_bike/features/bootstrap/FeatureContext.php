<?php

use App\Entity\Bike;
use App\Entity\Frame;
use App\Entity\Handlebar;
use App\Entity\Wheel;
use Behat\Behat\Context\Context;
use Behat\MinkExtension\Context\RawMinkContext;
use Imbo\BehatApiExtension\Context\ApiContext;
use Symfony\Component\HttpKernel\KernelInterface;
use Behat\MinkExtension;


    /**
 * This context class contains the definitions of the steps used by the demo
 * feature file. Learn how to get started with Behat and BDD on Behat's website.
 *
 * @see http://behat.org/en/latest/quick_start.html
 */
class FeatureContext extends ApiContext implements Context
{

    private $bike;


    /**
     * @var KernelInterface
     */
    protected $kernel;

    public function __construct(KernelInterface $kernel)
    {
        $this->bike = new Bike();
        $this->kernel = $kernel;
    }

    /**
     * @Given je choisi une roue avant du velo en :arg0 avec un prix de :arg1 €
     */
    public function jeChoisiUneRoueAvantDuVeloEnAvecUnPrixDeEur($arg0, $arg1)
    {
        $frontWheel = new Wheel();
        $frontWheel->setMaterial($arg0);
        $frontWheel->setPrice($arg1);
        $this->bike->setFrontWheel($frontWheel);
    }

    /**
     * @Given je choisi une roue arrière du velo en :arg0 avec un prix de :arg1 €
     */
    public function jeChoisiUneRoueArriereDuVeloEnAvecUnPrixDeEur($arg0, $arg1)
    {
        $backWheel = new Wheel();
        $backWheel->setMaterial($arg0);
        $backWheel->setPrice($arg1);
        $this->bike->setBackWheel($backWheel);
    }

    /**
     * @Given je choisi un cadre du velo en :arg0 avec un prix de :arg1 €
     */
    public function jeChoisiUnCadreDuVeloEnAvecUnPrixDeEur($arg0, $arg1)
    {
        $frame = new Frame();
        $frame->setMaterial($arg0);
        $frame->setPrice($arg1);
        $this->bike->setFrame($frame);
    }

    /**
     * @Given je choisi un guidon du velo en :arg0 avec un prix de :arg1 €
     */
    public function jeChoisiUnGuidonDuVeloEnAvecUnPrixDeEur($arg0, $arg1)
    {
        $handlebar = new Handlebar();
        $handlebar->setMaterial($arg0);
        $handlebar->setPrice($arg1);
        $this->bike->setHandlebar($handlebar);
    }

    /**
     * @Then j'obtiens un total de :arg1 €
     */
    public function jobtiensUnTotalDeEur($arg1)
    {
        if ($this->bike->getTotal() != $arg1) {
            throw new Exception("Le total ne correspond pas au prix attendu");
        }
    }

    /**
     * @return Bike
     */
    public function getBike(): Bike
    {
        return $this->bike;
    }


}