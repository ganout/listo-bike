# language: fr
Fonctionnalité: Calcul du prix total du vélo

  Scénario: création d'un vélo avec ses différents composents
    Etant donné que je choisi une roue avant du velo en aluminium avec un prix de 13.99 €
    Et que je choisi une roue arrière du velo en aluminium avec un prix de 13.99 €
    Et que je choisi un cadre du velo en aluminium avec un prix de 54.99 €
    Et que je choisi un guidon du velo en aluminium avec un prix de 28.99 €
    Alors j'obtiens un total de 111.96 €