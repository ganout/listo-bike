<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\BikeRepository;
use Doctrine\ORM\Mapping as ORM;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
/**
 * @ApiResource(
 *     collectionOperations={"get", "post"},
 *     itemOperations={"get", "put", "patch"}
 * )
 * @ORM\Entity(repositoryClass=BikeRepository::class)
 *
 */
class Bike
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=Wheel::class)
     * @ORM\JoinColumn(nullable=false)
     * @ApiFilter(SearchFilter::class, properties={"frontWheel.material":"ipartial"})
     */
    private $frontWheel;

    /**
     * @ORM\ManyToOne(targetEntity=Wheel::class)
     * @ORM\JoinColumn(nullable=false)
     * @ApiFilter(SearchFilter::class, properties={"backWheel.material":"ipartial"})
     */
    private $backWheel;

    /**
     * @ORM\ManyToOne(targetEntity=Frame::class)
     * @ORM\JoinColumn(nullable=false)
     * @ApiFilter(SearchFilter::class, properties={"frame.material":"ipartial"})
     */
    private $frame;

    /**
     * @ORM\ManyToOne(targetEntity=Handlebar::class)
     * @ORM\JoinColumn(nullable=false)
     * @ApiFilter(SearchFilter::class, properties={"handlebar.material":"ipartial"})
     */
    private $handlebar;

    /**
     * @ORM\Column(type="string", length=255)
     * @ApiFilter(SearchFilter::class, properties={"name":"ipartial"})
     */
    private $name;

    /**
     * @ORM\Column(type="float")
     */
    private $total;

    public function __construct()
    {

    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getFrontWheel(): ?Wheel
    {
        return $this->frontWheel;
    }

    public function setFrontWheel(?Wheel $frontWheel): self
    {
        $this->frontWheel = $frontWheel;
        $this->total += $frontWheel->getPrice();

        return $this;
    }

    public function getBackWheel(): ?Wheel
    {
        return $this->backWheel;
    }

    public function setBackWheel(?Wheel $backWheel): self
    {
        $this->backWheel = $backWheel;
        $this->total += $backWheel->getPrice();

        return $this;
    }

    public function getFrame(): ?Frame
    {
        return $this->frame;
    }

    public function setFrame(?Frame $frame): self
    {
        $this->frame = $frame;
        $this->total += $frame->getPrice();

        return $this;
    }

    public function getHandlebar(): ?Handlebar
    {
        return $this->handlebar;
    }

    public function setHandlebar(?Handlebar $handlebar): self
    {
        $this->handlebar = $handlebar;
        $this->total += $handlebar->getPrice();

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getTotal(): ?float
    {
        return $this->total;
    }

/*    public function setTotal(float $total): self
    {
        $this->total = $total;

        return $this;
    }*/
}
