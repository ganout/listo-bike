<?php

namespace App\Service;

use App\Entity\Bike;

class BillService
{

    public function billGenerator(Bike $bike) :array
    {
        return ["total" => (string) ($bike->getTotal() * 1.2)];
    }
}