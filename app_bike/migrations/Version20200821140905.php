<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200821140905 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP SEQUENCE handle_bar_id_seq CASCADE');
        $this->addSql('CREATE SEQUENCE bike_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE frame_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE handlebar_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE wheel_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE bike (id INT NOT NULL, front_wheel_id INT NOT NULL, back_wheel_id INT NOT NULL, frame_id INT NOT NULL, handlebar_id INT NOT NULL, name VARCHAR(255) NOT NULL, total DOUBLE PRECISION NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_4CBC37808AF639D3 ON bike (front_wheel_id)');
        $this->addSql('CREATE INDEX IDX_4CBC3780602120AF ON bike (back_wheel_id)');
        $this->addSql('CREATE INDEX IDX_4CBC37803FA3C347 ON bike (frame_id)');
        $this->addSql('CREATE INDEX IDX_4CBC3780EDE22325 ON bike (handlebar_id)');
        $this->addSql('CREATE TABLE frame (id INT NOT NULL, material VARCHAR(255) NOT NULL, price DOUBLE PRECISION NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE handlebar (id INT NOT NULL, material VARCHAR(255) NOT NULL, price DOUBLE PRECISION NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE wheel (id INT NOT NULL, material VARCHAR(255) NOT NULL, price DOUBLE PRECISION NOT NULL, PRIMARY KEY(id))');
        $this->addSql('ALTER TABLE bike ADD CONSTRAINT FK_4CBC37808AF639D3 FOREIGN KEY (front_wheel_id) REFERENCES wheel (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE bike ADD CONSTRAINT FK_4CBC3780602120AF FOREIGN KEY (back_wheel_id) REFERENCES wheel (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE bike ADD CONSTRAINT FK_4CBC37803FA3C347 FOREIGN KEY (frame_id) REFERENCES frame (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE bike ADD CONSTRAINT FK_4CBC3780EDE22325 FOREIGN KEY (handlebar_id) REFERENCES handlebar (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE bike DROP CONSTRAINT FK_4CBC37803FA3C347');
        $this->addSql('ALTER TABLE bike DROP CONSTRAINT FK_4CBC3780EDE22325');
        $this->addSql('ALTER TABLE bike DROP CONSTRAINT FK_4CBC37808AF639D3');
        $this->addSql('ALTER TABLE bike DROP CONSTRAINT FK_4CBC3780602120AF');
        $this->addSql('DROP SEQUENCE bike_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE frame_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE handlebar_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE wheel_id_seq CASCADE');
        $this->addSql('CREATE SEQUENCE handle_bar_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('DROP TABLE bike');
        $this->addSql('DROP TABLE frame');
        $this->addSql('DROP TABLE handlebar');
        $this->addSql('DROP TABLE wheel');
    }
}
